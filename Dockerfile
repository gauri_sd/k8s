FROM node:12

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install \ 
    && npm install aws-xray-sdk

COPY . .

EXPOSE 3000

CMD [ "node", "index.js" ]
